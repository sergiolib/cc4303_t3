#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include "jsocket6.4.h"
#include "bufbox.h"
#include "Data-rtt.h"

#define MAX_QUEUE 100 /* buffers en boxes */
#define WIN_SZ_R 50
#define WIN_SZ_S 50
#define MAXTIMEOUT 3.00
#define MINTIMEOUT 0.05
#define MAX_SEQ 256

/* T2: Version con threads
 * Implementa Go Back N con threads, 
 * 256 números de secuencia (0-255)
 * WIN_SZ_S buffers en enviador, 1 buffer en receptor
 * WIN_SZ_S DEBE ser < 128
 * con RTT adaptable para hacerlo mas eficiente
 * con DCNT para aceptar ACKs retransmitidos
 * Fast Retransmit: cuando llega un tercer ACK duplicado, hago como si fuera un timeout
 */


int Data_debug = 0; /* para debugging */

static int Dsock = -1;
static pthread_mutex_t Dlock;
static pthread_cond_t  Dcond;
static pthread_t Drcvr_pid, Dsender_pid;
static unsigned char ack[DHDR] = {0, ACK, 0, 0};

static void *Dsender(void *ppp);
static void *Drcvr(void *ppp);


#define max(a, b) (((a) > (b))?(a):(b))

struct {
    BUFBOX *rbox, *wbox;
    unsigned char pending_buf[WIN_SZ_S][BUF_SIZE];
    unsigned char rcv_buf[WIN_SZ_R][BUF_SIZE];
    int pending_sz[WIN_SZ_S];
    int rcv_sz[WIN_SZ_R];
    int retries[WIN_SZ_S];
    double timeout[WIN_SZ_S];
    unsigned char first_w, next_w; /* window pointers */
    int full_win;                  /* signals fullwin to disambiguate when first==next */
    unsigned char expected_ack, next_seq;
    int state;
    int id;
    double rtt, rdev, rtimeout, stime[WIN_SZ_S];
    int dup;            /* contador de DATA duplicados, para estadística */
    unsigned char marcado[WIN_SZ_S]; /* paquetes de la ventana de envio que reciben ACKs, vale 1 si recibio su ACK */
    unsigned char unexpected_acks;/*contador de ACKs para gatillar el fast retransmit */
    unsigned char fast_retransmit; /* flag en 1 para gatillar el fast retransmit */
    unsigned char first_r, next_r; /* punteros a la ventana de recepcion */
    unsigned char lfr, laf; /* numeros de sequencia que determinan la ventana de recpecion */
    unsigned char recibido[WIN_SZ_R]; /* frames de la ventana de recepcion que ya fueron recibidos */
  unsigned char old_fast_retransmit;
} connection;

/* Funciones utilitarias */

/* retorna la hora actual */
double Now() {
    struct timespec tt;
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    tt.tv_sec = mts.tv_sec;
    tt.tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, &tt);
#endif

    return(tt.tv_sec+1e-9*tt.tv_nsec);
}

/* Revisa rango cíclico */
int between(int x, int min, int max) {      /* min <= x < max */
    if(min <= max) return (min <= x && x < max);
    else          return (min <= x || x < max);
}

/* Retornar 1 si la ventana de envio está vacía */
int empty_sending_window() {
  return (connection.first_w == connection.next_w) && !connection.full_win;
}

/* debugging ventana */
void print_win() {
    int p;
    int cnt, prev;

    if(empty_sending_window()) {
        fprintf(stderr, "empty win\n");
        return;
    }
    fprintf(stderr, "win:");
    p=connection.first_w;
    cnt=0;
    do {
        cnt++;
/* Si quieren ver TODA la ventana descomentar esta linea */
        fprintf(stderr, " %d/%d", connection.pending_buf[p][DSEQ], connection.marcado[p]);
        if(cnt != 1 && (prev+1)%MAX_SEQ != connection.pending_buf[p][DSEQ]) {
            fprintf(stderr, "OJO: Out of order!\n");
            exit(1);
        }
        prev = connection.pending_buf[p][DSEQ];
        p = (p+1)%WIN_SZ_S;
    } while(p != connection.next_w);
    fprintf(stderr, " %d\n", cnt);
    if(cnt > WIN_SZ_S) { fprintf(stderr, "OJO: WIN TOO BIG!\n"); exit(1);}
}

void print_win_rcv() {
    int p;
    int cnt, prev;
    unsigned char seq;

    fprintf(stderr, "win_rcv:");
    p=connection.first_r;
    seq=connection.rcv_buf[p][DSEQ];
    cnt=0; //cantidad de frames de la ventana que fueron recibidos en desorden
    do {
	if(connection.recibido[p]) cnt++;
        fprintf(stderr, " %d/%d",seq, connection.recibido[p]);
	seq=(seq+1)%MAX_SEQ;
        p = (p+1)%WIN_SZ_R;
    } while(p != connection.next_r);
    fprintf(stderr, " %d\n", cnt);
    if(cnt > WIN_SZ_R) { fprintf(stderr, "OJO: WIN TOO BIG!\n"); exit(1);}
}

/* Inicializa estructura conexión */
int init_connection(int id) {
    int i;

    connection.state = CONNECTED;
    connection.wbox = create_bufbox(MAX_QUEUE); 
    connection.rbox = create_bufbox(MAX_QUEUE);
    for(i=0; i < WIN_SZ_S; i++) {
        connection.pending_sz[i] = -1;
	connection.marcado[i] = 0;
    }
    connection.expected_ack = 1; // LAR+1=1
    connection.next_seq = 1; //primer paquete que tengo que enviar es el 1
    connection.first_w = connection.next_w = 0; //punteros a la ventana
    connection.full_win = 0;
    connection.rtt = 3.0; /* 1 s */
    connection.rdev = 0.0; //medida de la varianza
    connection.rtimeout = connection.rtt + connection.rdev;
    connection.dup = 0;
    connection.id = id;    
    connection.fast_retransmit = 0;
    connection.unexpected_acks = 0;
    connection.first_r = 0;
    connection.next_r = WIN_SZ_R-1;
    connection.lfr = 0;
    connection.laf = WIN_SZ_R-1;
    for(i=0; i < WIN_SZ_R; i++){
      connection.rcv_sz[i] = -1;
      connection.rcv_buf[i][DSEQ] = i;
      connection.recibido[i] = 0;
    }
    connection.old_fast_retransmit = 0;
    return id;
}

/* borra estructura conexión */
void del_connection() {
    delete_bufbox(connection.wbox);
    delete_bufbox(connection.rbox);
    connection.state = FREE;
fprintf(stderr, "Freeing connection %d\n", connection.id);
}

/* Función que inicializa los threads necesarios: sender y rcvr */
static void Init_Dlayer(int s) {

    Dsock = s;
    if(pthread_mutex_init(&Dlock, NULL) != 0) fprintf(stderr, "mutex NO\n");
    if(pthread_cond_init(&Dcond, NULL) != 0)  fprintf(stderr, "cond NO\n");
    pthread_create(&Dsender_pid, NULL, Dsender, NULL);
    pthread_create(&Drcvr_pid, NULL, Drcvr, NULL);
}

/* timer para el timeout */
void tick() {
    return;
}

/* Función que me conecta al servidor e inicializa el mundo */
int Dconnect(char *server, char *port) {
    int s, cl, i;
    struct sigaction new, old;
    unsigned char inbuf[DHDR], outbuf[DHDR];

    if(Dsock != -1) return -1;

    s = j_socket_udp_connect(server, port);
    if(s < 0) return s;

/* inicializar conexion */
    bzero(&new, sizeof new);
    new.sa_flags = 0;
    new.sa_handler = tick;
    sigaction(SIGALRM, &new, &old);

    outbuf[DTYPE] = CONNECT;
    outbuf[DID] = 0;
    outbuf[DSEQ] = 0;
    outbuf[DCNT] = 1;
    for(i=0; i < RETRIES; i++) {
        send(s, outbuf, DHDR, 0);
	outbuf[DCNT]++;
	alarm(INTTIMEOUT);
	if(recv(s, inbuf, DHDR, 0) != DHDR) continue;
	if(Data_debug) fprintf(stderr, "recibo: %c, %d\n", inbuf[DTYPE], inbuf[DID]);
	alarm(0);
	if(inbuf[DTYPE] != ACK || inbuf[DSEQ] != 0) continue;
	cl = inbuf[DID];
	break;
    }
    sigaction(SIGALRM, &old, NULL);
    if(i == RETRIES) {
	fprintf(stderr, "no pude conectarme\n");
	return -1;
    }
fprintf(stderr, "conectado, id=%d\n", cl);
    init_connection(cl);
    Init_Dlayer(s); /* Inicializa y crea thread rcvr */
    return cl;
}

/* Lectura */
int Dread(int cl, char *buf, int l) {
int cnt;

    if(connection.id != cl) return -1;

    cnt = getbox(connection.rbox, buf, l);
    return cnt;
}

/* escritura */
void Dwrite(int cl, char *buf, int l) {
    if(connection.id != cl || connection.state != CONNECTED) return;

    putbox(connection.wbox, buf, l);
/* el lock parece innecesario, pero se necesita:
 * nos asegura que Dsender está esperando el lock o el wait
 * y no va a perder el signal! 
 */
    pthread_mutex_lock(&Dlock);
    pthread_cond_signal(&Dcond);	/* Le aviso a sender que puse datos para él en wbox */
    pthread_mutex_unlock(&Dlock);
}
/* cierra conexión */
void Dclose(int cl) {
    if(connection.id != cl) return;

    close_bufbox(connection.wbox);
    close_bufbox(connection.rbox);
}

/*
 * Aquí está toda la inteligencia del sistema: 
 * 2 threads: receptor desde el socket y enviador al socket 
 */

/* lector del socket: todos los paquetes entrantes */
static void *Drcvr(void *ppp) { 
    int cnt;
    int cl, p, i;
    unsigned char inbuf[BUF_SIZE];
    int found;
    double diff;
    unsigned char in_window;
    unsigned char recv_eof = 0;

/* No verifico que me estén inyectando datos falsos de una conexion */
    while((cnt=recv(Dsock, inbuf, BUF_SIZE, 0)) > 0) {
   	if(Data_debug)
	    fprintf(stderr, "recv: id=%d, type=%c, seq=%d, retries=%d\n", inbuf[DID], inbuf[DTYPE], inbuf[DSEQ], inbuf[DCNT]);
	if(cnt < DHDR) continue;

	cl = inbuf[DID];
	if(cl != connection.id) continue;

	pthread_mutex_lock(&Dlock);

	if(inbuf[DTYPE] == CLOSE) {
	  recv_eof = 1;
	}
	fprintf(stderr, "LFR: %d\n", connection.lfr);
	if (recv_eof == 1 && inbuf[DSEQ] == (connection.lfr+1)%MAX_SEQ) {  /* muy parecido a DATA */
	  if(Data_debug) fprintf(stderr, "rcv: CLOSE: %d, seq=%d, expected=%d\n\n", cl, inbuf[DSEQ], (connection.lfr + 1)%MAX_SEQ);
	  /* puede ocurrir que me manden un CLOSE pero que existan paquetes intermedios (inbu[DTYPE]==DATA) que no hayan llegado aun, por lo que se deberia chequear esto antes de cerrar la conexion */
	  /* Envio ack en todos los casos */
	  ack[DID] = cl;
	  ack[DTYPE] = ACK;
	  if(inbuf[DSEQ] == (connection.lfr + 1)%MAX_SEQ) {
	    /* si llego el CLOSE y todos los paquetes anteriores fueron recibidos */
	    ack[DSEQ] = inbuf[DSEQ];
	    ack[DCNT] = inbuf[DCNT];
	  }
	  else {
	    ack[DSEQ] = (connection.lfr);
	    ack[DCNT] = 1;
	  }
	  
	  if(Data_debug) fprintf(stderr, "Enviando CLOSE ACK %d, seq=%d\n", ack[DID], ack[DSEQ]);
	  
	  if(send(Dsock, ack, DHDR, 0) < 0) 
	    perror("sendack");
	  
	  if(inbuf[DSEQ] != (connection.lfr+1)%MAX_SEQ) { /* ignoramos el close */
	    pthread_mutex_unlock(&Dlock);
	    continue;
	  }
	  
	  connection.state = CLOSED;
	  fprintf(stderr, "closing %d, dups=%d\n", cl, connection.dup);
	  Dclose(cl);
	}
/* Un Ack: trabajamos para el enviador */
	else if(inbuf[DTYPE] == ACK && connection.state != FREE
                && between(inbuf[DSEQ], connection.expected_ack, connection.next_seq)) {
        /* liberar buffers entre expected_ack e inbuf[DSEQ] */

	    if(Data_debug)
		fprintf(stderr, "recv ACK id=%d, seq=%u, retries=%d\n", inbuf[DID], inbuf[DSEQ], inbuf[DCNT]);

            if(empty_sending_window()) {
                fprintf(stderr, "OJO: ack aceptado con ventana vacia!\n");
                exit(1);
            }
            found = 0;
            p=connection.first_w;
            do {
                if(connection.pending_buf[p][DSEQ] == inbuf[DSEQ]) {
                    found = 1;
		    connection.marcado[p] = 1;
                    break;
                }
                p=(p+1)%WIN_SZ_S;
            } while(p != connection.next_w);

            if(!found) {
                fprintf(stderr, "OJO: No pudo pasar: ack aceptado con ventana vacia?\n");
                exit(1);
            }

	    if(p != connection.first_w && connection.old_fast_retransmit == 0) {

	      connection.unexpected_acks++; //para el fast retransmit
	      if (connection.unexpected_acks > 2) {
		connection.fast_retransmit = 1;
		connection.old_fast_retransmit = 1;
	      }

            }
            if(inbuf[DCNT] == connection.retries[p]){ //los timeouts se actualizan cuando me llega un ACK de la ultima version del paquete que envie (por eso tengo que revisar su numero de retries)
	      diff = connection.rtt-(Now()-connection.stime[p]);
	      connection.rtt -= diff*0.125;
	      connection.rdev = 0.75*connection.rdev + (fabs(diff))*0.25;
	      connection.rtimeout = (connection.rtt + 4*connection.rdev)*1.1;
	      if(connection.rtimeout < MINTIMEOUT)
		connection.rtimeout = MINTIMEOUT;
	      if(connection.rtimeout > MAXTIMEOUT)
		connection.rtimeout = MAXTIMEOUT;
	      if(Data_debug) fprintf(stderr, "rtt=%f, diff=%f, rdev=%f, setting timeout to: %f, retries = %d\n", (Now()-connection.stime[p]), diff, connection.rdev, connection.rtimeout, inbuf[DCNT]);
	     }else if(Data_debug) fprintf(stderr, "rtt ignored, expected_retry=%d, received=%d\n", connection.retries[p], inbuf[DCNT]);
	    
	    
            if (p == connection.first_w) { //mover la ventana
	      connection.old_fast_retransmit = 0;
	      do {
		connection.marcado[p] = 0;
		p = connection.first_w = (connection.first_w+1)%WIN_SZ_S;
		connection.full_win = 0;
		connection.expected_ack = (connection.expected_ack+1)%MAX_SEQ;
	      }
	      while (connection.marcado[p]);
	    }

            if(connection.state == CLOSED &&
               empty_sending_window()) {
                /* conexion cerrada y sin buffers pendientes */
                del_connection();
            }
            if(Data_debug) print_win();
	    pthread_cond_signal(&Dcond);
	}
	else if(inbuf[DTYPE] == DATA && connection.state == CONNECTED) {
	  if(Data_debug) fprintf(stderr, "rcv: DATA: %d, seq=%d, expected from %d to %d\n", inbuf[DID], inbuf[DSEQ], (connection.lfr+1)%MAX_SEQ, connection.laf);
	    if(boxsz(connection.rbox) >= MAX_QUEUE) { /* No tengo espacio */
		pthread_mutex_unlock(&Dlock);
		continue;
	    }

	    if (connection.lfr == connection.laf) {
		pthread_mutex_unlock(&Dlock);
		continue;	      
	    }
	/* envio ack en todos los otros casos */
	    ack[DID] = cl;
	    ack[DTYPE] = ACK;
	    ack[DSEQ] = inbuf[DSEQ];
	    ack[DCNT] = inbuf[DCNT];

	    if (between(inbuf[DSEQ], (connection.lfr+1)%MAX_SEQ, (connection.laf+1)%MAX_SEQ)) { // si cabe dentro de la ventana, tengo que guardar una copia del frame en el buffer
	        unsigned char tmp; //la diferencia entre inbuf[DSEQ]-connection.lfr podria dar desborde
		if(connection.lfr < inbuf[DSEQ]){ tmp = (inbuf[DSEQ]-connection.lfr-1+MAX_SEQ)%MAX_SEQ;}
		else {tmp = ((MAX_SEQ-connection.lfr) + (inbuf[DSEQ])-1+MAX_SEQ)%MAX_SEQ;}
	        fprintf(stderr,"tmp : %d\n",tmp);
		connection.rcv_sz[connection.first_r+tmp] = cnt;//guardo la cantidad de bytes recibidos 
		fprintf(stderr,"rcv_sz: %d\n", connection.rcv_sz[connection.first_r+tmp]);
		connection.recibido[connection.first_r+tmp] = 1; //lo marco como frame recibido
		for (i = 0; i < cnt; i++) // guardo los bytes en el buffer
		  *((unsigned char *)(connection.rcv_buf[(connection.first_r+tmp)%WIN_SZ_R]+i))=*((unsigned char *)(inbuf+i));
		in_window = 1;
		//	        if(Data_debug) print_win_rcv();
	    } 
	    else { // si no cabe dentro de la ventana, decirle al sender q me llegó
	      in_window = 0;
	    }
	    if(inbuf[DSEQ] == (connection.lfr+1)%MAX_SEQ) { //muevo la ventana de recepcion y le paso el frame a la aplicacion
	      fprintf(stderr, "entré!\n");
	      do {
	      	connection.lfr = (connection.lfr+1)%MAX_SEQ;
		connection.laf = (connection.laf+1)%MAX_SEQ;
		fprintf(stderr, "%d %d\n", connection.rcv_buf[connection.first_r][DSEQ], connection.rcv_sz[connection.first_r]-DHDR);
		putbox(connection.rbox, connection.rcv_buf[connection.first_r]+DHDR, connection.rcv_sz[connection.first_r]-DHDR);//Se lo paso a la aplicacion
	        connection.rcv_sz[connection.first_r]=-1; //restauro los valores del buffer
		connection.recibido[connection.first_r] = 0; //ahora este espacio puede ser ocupado por otro frame
		connection.first_r = (connection.first_r+1)%WIN_SZ_R;
		connection.rcv_buf[connection.first_r][DSEQ] = connection.lfr;
		connection.next_r = (connection.next_r+1)%WIN_SZ_R;
		connection.rcv_buf[connection.next_r][DSEQ] = connection.laf+1;
		fprintf(stderr, "num_seq_first_r = %d\n", connection.rcv_buf[connection.first_r][DSEQ]);
	      } while (connection.recibido[connection.first_r]);
	    }
	    
	    if(Data_debug) fprintf(stderr, "Enviando ACK %d, seq=%d, retry=%d\n", ack[DID], ack[DSEQ], ack[DCNT]);

	    if(send(Dsock, ack, DHDR, 0) <0)
		perror("sendack");

            if(!in_window) {
		connection.dup++;
		if(Data_debug)
		  fprintf(stderr, "DUP DATA seq %d, expected between %d and %d, DUP=%d\n", inbuf[DSEQ], (connection.lfr+1)%MAX_SEQ, connection.laf, connection.dup);
	    }
	    //	    if(Data_debug) print_win_rcv();
        }
        else {
	    if(Data_debug) {
/* ver si es un ACK fuera de rango y en ese caso asumir un NACK y retransmitir
   la ventana! */
                fprintf(stderr, "descarto paquete entrante: t=%c, id=%d, s=%d\n", inbuf[DTYPE], cl, inbuf[DSEQ]);
	    }
        }

	pthread_mutex_unlock(&Dlock);
    }
    fprintf(stderr, "fallo read en Drcvr()\n");
    return NULL;
}

int client_retransmit() {
    int p;

    if(empty_sending_window())
        return 0; /* empty window */

    if(connection.fast_retransmit == 1) {
      fprintf(stderr, "Generando retransmisión\n");
      connection.fast_retransmit = 0;
      connection.unexpected_acks = 0;
      return 1;
    }

    p=connection.first_w;
    do {
      if (connection.marcado[p] == 1) {
	p=(p+1)%WIN_SZ_S;
	continue;
      }
      else {
	if(connection.timeout[p] <= Now()) {
            if(Data_debug) fprintf(stderr, "timeout pack: %d, tout=%f, now=%f\n", connection.pending_buf[p][DSEQ], connection.timeout[p], Now());
            return 1;
        }
        p=(p+1)%WIN_SZ_S;
      }
    } while(p != connection.next_w);

    return 0;
}

double Dclient_timeout_or_pending_data() {
    int cl, p;
    double timeout;
/* Suponemos lock ya tomado! */

    timeout = Now()+20.0;
    if(connection.state == FREE) return timeout;

    if(boxsz(connection.wbox) != 0 && !connection.full_win)
	/* data from client */
	return Now();

    if(empty_sending_window())
	return timeout; /* nothing pending: empty window */

    p=connection.first_w;
    do {
	if(connection.timeout[p] <= Now()) return Now();
	if(connection.timeout[p] < timeout) timeout = connection.timeout[p];
 	p=(p+1)%WIN_SZ_S;
    } while(p != connection.next_w);

    return timeout;
}

/* Thread enviador y retransmisor */
static void *Dsender(void *ppp) { 
    double timeout;
    struct timespec tt;
    int i;
    int p;
    int ret;
  
    for(;;) {
	pthread_mutex_lock(&Dlock);
        /* Esperar que pase algo */
	while((timeout=Dclient_timeout_or_pending_data()) > Now()) {
 // fprintf(stderr, "timeout=%f, now=%f\n", timeout, Now());
 // fprintf(stderr, "Al tuto %f segundos\n", timeout-Now());
	    tt.tv_sec = timeout;
 // fprintf(stderr, "Al tuto %f nanos\n", (timeout-tt.tv_sec*1.0));
	    tt.tv_nsec = (timeout-tt.tv_sec*1.0)*1000000000;
 // fprintf(stderr, "Al tuto %f segundos, %d secs, %d nanos\n", timeout-Now(), tt.tv_sec, tt.tv_nsec);
	    ret=pthread_cond_timedwait(&Dcond, &Dlock, &tt);
 // fprintf(stderr, "volvi del tuto con %d, now=%f\n", ret, Now());
	}

	/* Revisar: timeouts y datos entrantes */

	    if(connection.state == FREE) pthread_exit(0);
            if(client_retransmit()) { /* retransmitir toda la ventana */
            if(Data_debug) fprintf(stderr, "TIMEOUT: exp_ack=%d, next_seq=%d\n", connection.expected_ack, connection.next_seq);
            p=connection.first_w;
            do {
	      
	      if (connection.marcado[p] == 1) {
		p=(p+1)%WIN_SZ_S;
		continue;
	      }
	      
	      // Poner esto hace que se caiga todo
//Es necesario poner esto para actualizar los retries del paquete 
                /* if(connection.retries[p]++ > RETRIES) { */
                /*     fprintf(stderr, "%d: too many retries: %d, seq=%d\n", connection.id, connection.retries[p], connection.pending_buf[p][DSEQ]); */
                /*     del_connection(); */
                /*     pthread_exit(0); */
                /* } */
		connection.pending_buf[p][DCNT] = connection.retries[p];
                if(Data_debug) fprintf(stderr, "Re-send DATA, cl=%d, seq=%d, retries=%d\n", connection.id, connection.pending_buf[p][DSEQ], connection.pending_buf[p][DCNT]);
                if(send(Dsock, connection.pending_buf[p], DHDR+connection.pending_sz[p], 0) < 0) {
                    perror("sendto1"); exit(1);
                }

		if(p == connection.first_w) { /* first time */
		    connection.rtimeout *= 2;
		    if(connection.rtimeout > MAXTIMEOUT)
		    	connection.rtimeout = MAXTIMEOUT;

		    if(Data_debug) fprintf(stderr, "Dup timeout=%f\n", connection.rtimeout);
		}

		connection.stime[p] = Now();
		connection.timeout[p] = connection.stime[p] + connection.rtimeout;

                p=(p+1)%WIN_SZ_S;
            } while(p != connection.next_w);
            }
            if(boxsz(connection.wbox) != 0 && !connection.full_win) {
/*
                Hay un buffer para mi para enviar
                leerlo, enviarlo, marcar esperando ACK
*/
	        p = connection.next_w;
                connection.pending_sz[p] = getbox(connection.wbox, (char *)connection.pending_buf[p]+DHDR, BUF_SIZE);
                connection.pending_buf[p][DID]=connection.id;
                connection.pending_buf[p][DSEQ]=connection.next_seq;
                connection.pending_buf[p][DCNT]=1;
                connection.next_seq = (connection.next_seq+1)%MAX_SEQ;
                connection.next_w = (connection.next_w+1)%WIN_SZ_S;
                if(connection.next_w == connection.first_w)
                    connection.full_win = 1;

                if(connection.pending_sz[p] == -1) { /* EOF */
		  if(Data_debug) fprintf(stderr, "sending EOF\n");
                   connection.state = CLOSED;
                   connection.pending_buf[p][DTYPE]=CLOSE;
                   connection.pending_sz[p] = 0; //para diferenciar el EOF de un paquete no inicializado 
		//para el EOF se manda solamente un header con el CLOSE
                   fprintf(stderr, "closing %d, dups=%d\n", connection.id, connection.dup);
                }
                else {
                   if(Data_debug)
                        fprintf(stderr, "sending DATA id=%d, seq=%d\n", connection.id, connection.pending_buf[p][DSEQ] );
                   connection.pending_buf[p][DTYPE]=DATA;
                }

                if(Data_debug) print_win();

                sendto(Dsock, connection.pending_buf[p], DHDR+connection.pending_sz[p], 0, NULL, 0);

		connection.stime[p] = Now();
		connection.timeout[p] = connection.stime[p] + connection.rtimeout;
		connection.retries[p] = 1;
	    }
	pthread_mutex_unlock(&Dlock);
    }
    return NULL; /* unreachable */
}
